import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxMarketplaceHomeComponent } from './ngx-marketplace-home.component';
import { NgxSidebarModule } from '@4geit/ngx-sidebar-component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';
import { NgxSlideshowModule } from '@4geit/ngx-slideshow-component';
import { NgxMarketplaceCatalogModule } from '@4geit/ngx-marketplace-catalog-component';
import { NgxCartButtonModule } from '@4geit/ngx-cart-button-component';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule,
    NgxSidebarModule,
    NgxSlideshowModule,
    NgxMarketplaceCatalogModule,
    NgxCartButtonModule,
  ],
  declarations: [
    NgxMarketplaceHomeComponent
  ],
  exports: [
    NgxMarketplaceHomeComponent
  ]
})
export class NgxMarketplaceHomeModule { }
