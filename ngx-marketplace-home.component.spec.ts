import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMarketplaceHomeComponent } from './ngx-marketplace-home.component';

describe('marketplace-home', () => {
  let component: marketplace-home;
  let fixture: ComponentFixture<marketplace-home>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ marketplace-home ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(marketplace-home);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
