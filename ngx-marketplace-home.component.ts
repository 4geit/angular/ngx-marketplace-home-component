import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-marketplace-home',
  template: require('pug-loader!./ngx-marketplace-home.component.pug')(),
  styleUrls: ['./ngx-marketplace-home.component.scss']
})
export class NgxMarketplaceHomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
